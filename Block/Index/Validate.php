<?php

namespace Magewares\MWBotBlocker\Block\Index;


class Validate extends \Magento\Framework\View\Element\Template {
	
	protected $mwbotConfigHelper;

    public function __construct(
	    \Magento\Catalog\Block\Product\Context $context, 
		\Magewares\MWBotBlocker\Helper\Config $mwbotConfigHelper,
		array $data = []
	) {
        $this->mwbotConfigHelper = $mwbotConfigHelper;
        parent::__construct($context, $data);

    }
   public function getActionUrl(){
	   $urlPost = $this->getUrl('mwbotblocker/index/validatepost');
	   return $urlPost;
   }
   
   public function getSiteKey(){
	  return $this->mwbotConfigHelper->getConfig('mwbotblocker/general/publickey'); 
   }
   
   public function addRecaptachJs(){
	  return '<script src="https://www.google.com/recaptcha/api.js"></script>'; 
   }
   

}