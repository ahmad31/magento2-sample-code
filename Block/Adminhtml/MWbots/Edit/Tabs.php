<?php
namespace Magewares\MWBotBlocker\Block\Adminhtml\MWbots\Edit;

/**
 * Admin page left menu
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('mwbots_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('MWbots Information'));
    }
}