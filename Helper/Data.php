<?php
namespace Magewares\MWBotBlocker\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
	protected $mwbotConfigHelper;
	protected $urlInterface;
	protected $scopeConfig;
	protected $_serverrequest;
    public function __construct(
	    \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
	    \Magewares\MWBotBlocker\Helper\Config $mwbotConfigHelper,
		\Magento\Framework\UrlInterface $urlInterface,
		\Magento\Framework\HTTP\PhpEnvironment\Request $serverrequest
	)
   {
      $this->scopeConfig = $scopeConfig;
      $this->mwbotConfigHelper = $mwbotConfigHelper;
      $this->urlInterface = $urlInterface;
      $this->_serverrequest = $serverrequest;
   }
   
   public function doesUrlContainsDisquallifiers(){
	   $isItTrue = false;
	   $urlFilters = $this->mwbotConfigHelper->getConfig('mwbotblocker/general/urlfilters');
	   if(trim($urlFilters) == false) return false;
	   $arrayOfRegExp = explode(';',$urlFilters); 
	   
	   foreach ($arrayOfRegExp as $regexp) {
	    	if (null !== $this->_serverrequest->getRequestUri() and @preg_match($regexp, $this->_serverrequest->getRequestUri())) {
	    		$isItTrue = true;
	    		break;
	    	}
	    }
		return $isItTrue;
   }
}