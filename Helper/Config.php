<?php
namespace Magewares\MWBotBlocker\Helper;

class Config extends \Magento\Framework\App\Helper\AbstractHelper
{
	protected $configObject;
    public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig)
   {
      $this->scopeConfig = $scopeConfig;
   }
   public function getConfig($tabGrpSectionField){
	   $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
	   return $this->scopeConfig->getValue($tabGrpSectionField);
   }

}