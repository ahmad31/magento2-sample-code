<?php

namespace Magewares\MWBotBlocker\Controller\Index;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Validatepost extends \Magento\Framework\App\Action\Action
{
	protected $mwbotConfigHelper;
	protected $MWbotsFactory;
	protected $logger;
	protected static $_siteVerifyUrl = "https://www.google.com/recaptcha/api/siteverify?";
	protected $_secret;
	protected static $_version = "php_1.0";
	
	protected $_serverrequest;
	public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
		\Magento\Framework\HTTP\PhpEnvironment\Request $serverrequest	
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->_serverrequest = $serverrequest;
    }

    public function execute()
    {
	   $this->mwbotConfigHelper = $this->_objectManager->create('\Magewares\MWBotBlocker\Helper\Config');
	   $this->MWbotsFactory = $this->_objectManager->create('\Magewares\MWBotBlocker\Model\MWbotsFactory');
	   $this->logger = $this->_objectManager->create('\Psr\Log\LoggerInterface');
       $storeManager = $this->_objectManager->get('\Magento\Store\Model\StoreManagerInterface');
       if(!$this->mwbotConfigHelper->getConfig('mwbotblocker/general/enable')){
		  return $this->_redirect($storeManager->getStore()->getBaseUrl()); 
	   }
	   $this->_secret = $this->mwbotConfigHelper->getConfig('mwbotblocker/general/secretkey');
	   $resultRedirect = $this->MWbotsFactory->create();
	   $captcha = $this->getRequest()->getParam('g-recaptcha-response');
       $secret = $this->_secret;
	   $response = null;
	   $path = self::$_siteVerifyUrl;
	   $dataCaptcha = array (
		                   'secret' => $secret,
		                   'remoteip' => $this->_serverrequest->getServerValue('REMOTE_ADDR'),
		                   'v' => self::$_version,
		                   'response' => $captcha
		            );
		$req = "";
		foreach ($dataCaptcha as $key => $value) {
			 $req .= $key . '=' . urlencode(stripslashes($value)) . '&';
		}
		$req = substr($req, 0, strlen($req)-1);
		$response = file_get_contents($path . $req);
		$answers = json_decode($response, true);
		if(trim($answers ['success']) == true) {
			$botsCollectionObject = $this->MWbotsFactory->create();
	        $botsCollection = $botsCollectionObject->getCollection()
						->addFieldToFilter('ip', $this->_serverrequest->getServerValue('REMOTE_ADDR'));
	        foreach($botsCollection as $bot){
			    try{
				    $bot->delete();
			    }catch(\Exception $e){
			        $this->logger->info($e->getMessage());	
			    }
			}
			$this->messageManager->addSuccess(__('We have identified you a human, you are free to access the site.'));
			$this->_redirect($storeManager->getStore()->getBaseUrl());
		}else{
			$this->messageManager->addError(__('Captcha is not correct, please try again.'));
			$this->_redirect($this->_redirect->getRefererUrl());
		}


    }
}