<?php

namespace Magewares\MWBotBlocker\Controller\Index;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Pot extends \Magento\Framework\App\Action\Action
{
	protected $mwbotConfigHelper;
	protected $MWbotsFactory;
	protected $logger;
	protected $_serverrequest;
	protected $_remoteaddress;
	public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
		\Magento\Framework\HTTP\PhpEnvironment\Request $serverrequest,
		\Magento\Framework\HTTP\PhpEnvironment\RemoteAddress $remoteaddress		
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->_serverrequest = $serverrequest;
        $this->_remoteaddress = $remoteaddress;
    }
    public function execute()
    {
		
	   $this->mwbotConfigHelper = $this->_objectManager->create('\Magewares\MWBotBlocker\Helper\Config');
	   $this->MWbotsFactory = $this->_objectManager->create('\Magewares\MWBotBlocker\Model\MWbotsFactory');
	   $this->urlInterface = $this->_objectManager->create('\Magento\Framework\UrlInterface');
	   $this->logger = $this->_objectManager->create('\Psr\Log\LoggerInterface');
       if(!$this->mwbotConfigHelper->getConfig('mwbotblocker/general/enable')) return;
	   $resultRedirect = $this->MWbotsFactory->create();
	   $botsCollectionObject = $this->MWbotsFactory->create();
	   $botsCollection = $botsCollectionObject->getCollection()
						->addFieldToFilter('ip', $this->_remoteaddress->getRemoteAddress());
		try {
			if ($this->mwbotConfigHelper->getConfig('mwbotblocker/general/hponlypost')) {
				if (empty($this->_serverrequest->getServerValue('REQUEST_METHOD')) == false and $this->_serverrequest->getServerValue('REQUEST_METHOD') != 'POST') {
					 return $this->_redirect($this->_redirect->getRefererUrl());
				}
			}
			
			$ipAddress = $this->_serverrequest->getServerValue('REMOTE_ADDR');
			$userAgent = $this->_serverrequest->getServerValue('HTTP_USER_AGENT');
			if (!$botsCollection->getSize()) {
				$status = 'enabled';
				if(!$this->mwbotConfigHelper->getConfig('mwbotblocker/general/defaultstatus')) $status = 'disabled';
				$banPeriod = $this->mwbotConfigHelper->getConfig('mwbotblocker/general/banperiod');
				if(!$banPeriod) $banPeriod = 1;
				$date = strtotime("+$banPeriod day", time());
                $banDate = date('Y-m-d H:i:s', $date);
				$bot = $this->MWbotsFactory->create();
				$bot->setStatus($status);
				$bot->setIp($ipAddress);
				$bot->setUserAgent($userAgent);
				$bot->setExpiresAt($banDate);
				$bot->setCreatedAt(date('Y-m-d H:i:s'));
				$bot->save();
			}
		} catch (\Exception $e) {
			$this->logger->info($e->getMessage());
		}
		
		return $this->_redirect($this->urlInterface->getUrl('mwbotblocker/index/validate'));
    }
}