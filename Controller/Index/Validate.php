<?php

namespace Magewares\MWBotBlocker\Controller\Index;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Validate extends \Magento\Framework\App\Action\Action
{
    protected $mwbotConfigHelper;
    protected $MWbotsFactory;
    protected $logger;
	protected $_serverrequest;
	protected $_remoteaddress;
	public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
		\Magento\Framework\HTTP\PhpEnvironment\RemoteAddress $remoteaddress		
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->_remoteaddress = $remoteaddress;
    }
    public function execute()
    {
	   $this->mwbotConfigHelper = $this->_objectManager->create('\Magewares\MWBotBlocker\Helper\Config');
	   $this->MWbotsFactory = $this->_objectManager->create('\Magewares\MWBotBlocker\Model\MWbotsFactory');
	   $this->logger = $this->_objectManager->create('\Psr\Log\LoggerInterface');
       if(!$this->mwbotConfigHelper->getConfig('mwbotblocker/general/enable')){
		  $storeManager = $this->_objectManager->get('\Magento\Store\Model\StoreManagerInterface');
		  return $this->_redirect($storeManager->getStore()->getBaseUrl()); 
	   }
	   $resultRedirect = $this->MWbotsFactory->create();
	   $botsCollectionObject = $this->MWbotsFactory->create();
	   $botsCollection = $botsCollectionObject->getCollection()
						->addFieldToFilter('ip', $this->_remoteaddress->getRemoteAddress());
	   $bot = $botsCollection->getFirstItem();
	   if ($bot->getBotId()) {
			try {
				$bot->setLastSeenAt(date('Y-m-d H:i:s'));
				$bot->setTotalVisits($bot->getTotalVisits() + 1);
				$bot->save();
				
			} catch (\Exception $e) {
				$this->logger->info($e->getMessage());
			}
			$this->_view->loadLayout();
            $this->_view->getLayout()->initMessages();
            $this->_view->renderLayout();
		}else{
			$storeManager = $this->_objectManager->get('\Magento\Store\Model\StoreManagerInterface');
			return $this->_redirect($storeManager->getStore()->getBaseUrl());
		}
    }
}