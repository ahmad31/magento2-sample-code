<?php
namespace Magewares\MWBotBlocker\Controller\Adminhtml\testasbot;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action
{
	protected $_serverrequest;
	public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
		\Magento\Framework\HTTP\PhpEnvironment\Request $serverrequest	
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->_serverrequest = $serverrequest;
    }
    public function execute()
    {
		$this->_storeManager = $this->_objectManager->create('\Magento\Store\Model\StoreManagerInterface');
		$this->mwbotConfigHelper = $this->_objectManager->create('\Magewares\MWBotBlocker\Helper\Config');
		$banPeriod = $this->mwbotConfigHelper->getConfig('mwbotblocker/general/banperiod');
		if(!$banPeriod) $banPeriod = 1;
		$date = strtotime("+$banPeriod day", time());
        $banDate = date('Y-m-d H:i:s', $date);
		$status = 'enabled';
		$ipAddress = $this->_serverrequest->getServerValue('REMOTE_ADDR');
		$userAgent = $this->_serverrequest->getServerValue('HTTP_USER_AGENT');
        $this->MWbotsFactory = $this->_objectManager->create('\Magewares\MWBotBlocker\Model\MWbotsFactory');
		$bot = $this->MWbotsFactory->create();
		$bot->setStatus($status);
		$bot->setIp($ipAddress);
		$bot->setUserAgent($userAgent);
		$bot->setExpiresAt($banDate);
		$bot->setCreatedAt(date('Y-m-d H:i:s'));
		$bot->save();
		return $this->_redirect($this->_storeManager->getStore()->getBaseUrl());
	}
}
