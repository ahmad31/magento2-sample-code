<?php
namespace Magewares\MWBotBlocker\Model;

class MWbots extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magewares\MWBotBlocker\Model\ResourceModel\MWbots');
    }
}
