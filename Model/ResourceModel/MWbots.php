<?php
namespace Magewares\MWBotBlocker\Model\ResourceModel;

class MWbots extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('mw_bots', 'bot_id');
    }
}
