<?php
namespace Magewares\MWBotBlocker\Observer;

class Sendresponsebefore implements \Magento\Framework\Event\ObserverInterface
{
   protected $mwbotConfigHelper;
   protected $mwbotDataHelper;
   protected $urlInterface;
   protected $MWbotsFactory;
   protected $responseFactory;
   protected $_serverrequest;
   
   public function __construct(
      \Magewares\MWBotBlocker\Helper\Config $mwbotConfigHelper,
      \Magewares\MWBotBlocker\Helper\Data $mwbotDataHelper,
      \Magento\Framework\UrlInterface $urlInterface,
      \Magewares\MWBotBlocker\Model\MWbotsFactory $MWbotsFactory,
	  \Magento\Framework\HTTP\PhpEnvironment\Request $serverrequest,
	  \Magento\Framework\App\ResponseFactory $responseFactory
  ){
	 $this->mwbotConfigHelper = $mwbotConfigHelper;
	 $this->mwbotDataHelper = $mwbotDataHelper;
	 $this->urlInterface = $urlInterface;
	 $this->MWbotsFactory = $MWbotsFactory;
	 $this->responseFactory = $responseFactory;
	 $this->_serverrequest = $serverrequest;
  }
  public function execute(\Magento\Framework\Event\Observer $observer)
  {
	if(!$this->mwbotConfigHelper->getConfig('mwbotblocker/general/enable')) return;
	  $uri = ltrim($this->urlInterface->getCurrentUrl(), '/');
	  if (strpos($uri, 'welcomb/') !== false) return;
	  if ($this->mwbotDataHelper->doesUrlContainsDisquallifiers()) return;
	  $botsCollectionObject = $this->MWbotsFactory->create();
	  $botsCollection = $botsCollectionObject->getCollection()
	                    ->addFieldToFilter('status', 'enabled')
						->addFieldToFilter('ip', $this->_serverrequest->getServerValue('REMOTE_ADDR'))
						->addFieldToFilter('expires_at', array('gteq' => date('Y-m-d H:i:s')));
	  $bot = $botsCollection->getFirstItem();
	  /****We have found the BOT, let it be checked********/
	  if($bot->getBotId()){
	      $redirectUrl = $this->urlInterface->getUrl('mwbotblocker/index/validate');
	      $observer->getResponse()->setRedirect($redirectUrl)->sendResponse();
	  }
	$html = '';
    $response = $observer->getEvent()->getData('response');
	$body = $response->getBody();
	if (($bodyEndPos = stripos($body, '</body>')) === false) {
			return;
	}
	if($this->mwbotConfigHelper->getConfig('mwbotblocker/general/hpform')){
		$html .= preg_replace('/\s+/s', ' ', sprintf('<div class="no-display">
				<form action="%s" method="post">
					<input type="text" name="name"/>
					<input type="text" name="email"/>
					<textarea name="message" rows="5" cols="40"></textarea>
				</form>
				<form action="%s" method="post">
					<input type="text" name="q"/>
					<input type=submit">
				</form>
			</div>', $this->urlInterface->getUrl('mwbotblocker/index/pot'), $this->urlInterface->getUrl('mwbotblocker/index/pot')));
	}
	if ($this->mwbotConfigHelper->getConfig('mwbotblocker/general/hplink') && !$this->mwbotConfigHelper->getConfig('mwbotblocker/general/hponlypost')) {
		$html .= preg_replace('/\s+/s', ' ', sprintf('<div class="no-display">
			<a href="%s" rel="nofollow">click here</a>
			</div>', $this->urlInterface->getUrl('mwbotblocker/index/pot')));
	}
	$body = substr($body, 0, $bodyEndPos) . $html . substr($body, $bodyEndPos);
    $response->setBody($body);
  }
}